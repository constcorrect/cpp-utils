// file      : cpp-utils/Export.hpp
// copyright : Copyright (c) 2019 Alexander Gnatyuk
// license   : MIT; see accompanying LICENSE file

#pragma once

#ifdef CPP_UTILS_USE_MODULES

#define CPP_UTILS_EXPORT_START export {
#define CPP_UTILS_EXPORT_END }

#else

#define CPP_UTILS_EXPORT_START
#define CPP_UTILS_EXPORT_END

#endif

