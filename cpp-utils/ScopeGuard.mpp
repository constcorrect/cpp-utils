// file      : cpp-utils/ScopeGuard.mpp
// copyright : Copyright (c) 2019 Alexander Gnatyuk
// license   : MIT; see accompanying LICENSE file

#ifndef __cpp_modules
#pragma once
#endif

#ifndef __cpp_lib_modules
#include <utility>
#endif

#ifdef __cpp_modules
export module CppUtils.ScopeGuard;

#ifdef __cpp_lib_modules
import std.core;
#endif // __cpp_lib_modules

#endif // __cpp_modules

#include <cpp-utils/Export.hpp>

CPP_UTILS_EXPORT_START

template <typename Destructor> class ScopeGuard {
public:
    ScopeGuard(Destructor&& d) : d{std::move(d)} {}
    ~ScopeGuard() { d(); }

    ScopeGuard() = delete;
    ScopeGuard(ScopeGuard const&) = delete;
    ScopeGuard& operator=(ScopeGuard const&) = delete;
    ScopeGuard(ScopeGuard&&) = delete;
    ScopeGuard& operator=(ScopeGuard&&) = delete;

private:
    Destructor const d;
};

template <typename Destructor> decltype(auto) makeScopeGuard(Destructor&& d) {
    return ScopeGuard<Destructor>(std::forward<Destructor>(d));
}

template <typename Destructor>
ScopeGuard(Destructor &&)->ScopeGuard<Destructor>;

CPP_UTILS_EXPORT_END
